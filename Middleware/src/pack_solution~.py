#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from head import *

#==============================================================
# This function is to send conmand of cdma online
#==============================================================
def send_command_cdma_online():
    handler.write(binascii.a2b_hex(d_frame["FRAME_H"]))
    handler.write(binascii.a2b_hex(d_frame["FRAME_L"]))
    handler.write(binascii.a2b_hex('0' + str(d_length["LEN_COMMAND"])))

    handler.write(binascii.a2b_hex(d_frame["FRAME_TYPE_COMMAND"]))
    handler.write(binascii.a2b_hex(d_command_type["CDMA_ONLINE"]))
    print "send cdma_online done"
    return d_return_mark["SEND_CDMA_ONLINE"]

#==============================================================
# This function is to send conmand of data OK
#==============================================================
def send_command_data_ok():
    handler.write(binascii.a2b_hex(d_frame["FRAME_H"]))
    handler.write(binascii.a2b_hex(d_frame["FRAME_L"]))
    handler.write(binascii.a2b_hex('0' + str(d_length["LEN_COMMAND"])))
    handler.write(binascii.a2b_hex(d_frame["FRAME_TYPE_COMMAND"]))
    handler.write(binascii.a2b_hex(d_command_type["DATA_OK"]))
    print "send data_ok done"
    return d_return_mark["SEND_DATA_OK"]

def transfor(two_byte):
    temp = int(two_byte)
    #if temp > 0 and temp < 16:
    if temp < 16:
        temp = hex(temp)[2:3]
        temp = '0' + temp
    #else temp >= 16 :
    else:
        temp = hex(temp)[2:4]
    #else:
        #return ''
    return temp
#==============================================================
# This function is to send data to update time on nodes
#==============================================================
def send_update_time():
    now_time = datetime.now()
    now_time = now_time.strftime('%y%m%d%H%M%S')
    #now_time = now_time[2:14]
    i = 0
    s_now_time = ''
    while i <= 10:
        s_now_time += transfor(now_time[i: i + 2])
        i += 2

    frame_h         = binascii.a2b_hex(d_frame["FRAME_H"])
    frame_l         = binascii.a2b_hex(d_frame["FRAME_L"])
    len_time_pack   = binascii.a2b_hex('0' + str(d_length["LEN_TIME_PACK"]))
    frame_type_data = binascii.a2b_hex(d_frame["FRAME_TYPE_DATA"])
    data_type_time  = binascii.a2b_hex(d_frame["DATA_TYPE_TIME"])
    s_now_time      = binascii.a2b_hex(s_now_time)
    handler.write(frame_h)
    handler.write(frame_l)
    handler.write(len_time_pack)
    handler.write(frame_type_data)
    handler.write(data_type_time)
    handler.write(s_now_time)

    ###############################
    # generate the confirm byte
    ###############################
    frame = frame_h + frame_l + len_time_pack + frame_type_data + data_type_time + s_now_time
    check_byte = generate_check_byte(frame)
    handler.write(binascii.a2b_hex(check_byte))
    print "sending update time frame: %s" %binascii.b2a_hex(frame) + check_byte
    print "send update_time done"
    return d_return_mark["SEND_UPDATE_DATATIME"]

#==============================================================
# This function is to check legality of the datas int the frame
#==============================================================
def beet_solution(beet, cup):
    print "READING A BEET PACK..."
    beet += cup
    cup = handler.read(d_length["LEN_BEET"] - len(beet))
    beet += cup
    return (beet, d_return_mark["BEET_PACK_READ_RIGHT"])

#==============================================================
# 此函数产生校验为
#==============================================================
def generate_check_byte(frame):
    frame_len = len(frame)
    result = 0
    try:
        for i in frame[2:frame_len]:
            result ^= int(binascii.b2a_hex(i), 16)
    except IndexError as index_error:
        print index_error
        return d_return_mark["INDEX_ERROR"]
    else:
        if result < 16:
            result = hex(result)
            result = '0' + result[2:]
        else:
            result = hex(result)
            result = result[2:4]
        return result

#==============================================================
# 此函数对帧进行帧校验
#==============================================================
def check_frame(frame):
    print "checking data..."
    frame_len = len(frame)
    try:
        check_byte = binascii.b2a_hex(frame[frame_len - 1])
        print "check_byte = %s" %check_byte
        result = generate_check_byte(frame[:frame_len - 1])
        print "result = %s" %result
        if result == check_byte:
            print "************legal frame!!!"
            return d_return_mark["CHECK_LEGAL_FRAME"]
        else:
            print "!!!!!!!!!illegal frame!!!!!!!!!"
            return d_return_mark["CHECK_ILLEGAL_FRAME"]
    except IndexError as index_error:
        print index_error
        return d_return_mark["INDEX_ERROR"]

#==============================================================
# This function is to analyse and divide the frame
#==============================================================
def frame_solution(frame, cup):
    print "GET A FRAME..."
    frame += cup
    cup = handler.read(2)
    if binascii.b2a_hex(cup[1]) == d_frame["FRAME_TYPE_COMMAND"]:        #命令帧
        print "GOT A COMMAND..."
        frame += cup
        cup = handler.read(2)
        if binascii.b2a_hex(cup) == d_command_type["DATA_OK"]:
            frame += cup
            print "GOT A DATA OK RESPONCE!"
            return d_return_mark["RECEIVE_DATA_OK"]
        elif binascii.b2a_hex(cup) == d_command_type["UPDATE_TIME"]:
            frame += cup
            print "GOT A ASK TO UPDATE TIME..."
            send_update_time()
            return d_return_mark["RECEIVE_UPDATE_DATATIME"]
        else:
            return d_return_mark["UNKNOWN_COMMAND"]
    elif binascii.b2a_hex(cup[1]) == d_frame["FRAME_TYPE_DATA"]:      #数据帧
        print "GOT A DATA PACK"
        frame += cup
        cup = handler.read(1)
        if binascii.b2a_hex(cup) == d_frame["DATA_TYPE_DATA"]:     #记录数据
            print "it's a record data!"
            i = 0
            n = int(binascii.b2a_hex(frame[2]),16)-3
            print "n = %d" %n
            frame += cup
            while i < n:
                cup = handler.read(3)
                print "the i = %d record" %i
                frame += cup
                j = 0
                m = int(binascii.b2a_hex(cup[2]),16)
                print m
                while j < m:
                    cup = handler.read(10)
                    frame += cup
                    print "the j = %d inner_record" %j
                    print binascii.b2a_hex(cup)
                    j = j + 1
                i = i + 1
            frame += handler.read(1)
            return frame
        else:
            print "UNKNOWN_DATA_TYPE"
            return d_return_mark["UNKNOWN_DATA_TYPE"]
    else:
        print "UNKNOWN_FRAME_TYPE"
        return d_return_mark["UNKNOWN_FRAME_TYPE"]


