#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
设置超时时间客户端
第一次测试将sleep的参数设置为10,服务器端显示为"timeout"
第二次测试将sleep的参数设置为2,服务器端显示为"message comes from timeout client"
"""
import socket
from head import HOST, PORT

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((HOST,PORT))
import time
# 运行两次,分别使用不同的时间值,指定大小依据server端的设置,现在服务端为5
# 测试时使用10和2,测试settimeout的作用
while 1:
    time.sleep(2)
    print "send one"
    s.send("message comes from timeout client" + "\r")
    time.sleep(3)
s.shutdown(socket.SHUT_WR)
s.close()