from client_head import *

def client_main():
    try:
        # Connect to server and send data
        sock.connect((HOST, PORT))
        while True:
            data = pack_beet
            sock.send(data)
            # Receive data from the server and shut down
            received = sock.recv(7)
            print "Sent beet pack       : ", data
            print "Received cdma online : " ,binascii.b2a_hex(received)

            print "============================================================"
            data = pack_update_time
            sock.send(binascii.a2b_hex(data))
            print "Sent command update time: " , data
            # Receive data from the server and shut down
            received = sock.recv(7)
            print "Received data ok and time: " , binascii.b2a_hex(received)
            received = sock.recv(12)
            print "Received data time : " , binascii.b2a_hex(received)
            data = pack_data_ok
            sock.send(binascii.a2b_hex(data))

            print "============================================================"
            data = pack_frame_data
            sock.send(binascii.a2b_hex(data))
            # Receive data from the server and shut down
            received = sock.recv(7)
            print "Sent light data frame  : " , data
            print "Received data ok : " , binascii.b2a_hex(received)

            print "============================================================"
            data = complex_frame
            sock.send(binascii.a2b_hex(data))
            # Receive data from the server and shut down
            received = sock.recv(7)
            print "Sent complex data frame  : " , data
            print "Received data ok : " , binascii.b2a_hex(received)

            print "============================================================"
            #sleep(3600)
            sleep(60)
    except KeyboardInterrupt as keyboard_interrupt:
        print "[KeyboardInterrupt] program is going to down...Thank you!!! "
    except socket.error as e:
        print e
    finally:
        print "client close socket"

if __name__ == "__main__":
    client_main()

