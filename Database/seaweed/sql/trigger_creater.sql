delimiter ;;
/*================================================*/
drop trigger if exists trg_bef_node_inserted;;
create trigger trg_bef_node_inserted before insert on tb_node
for each row 
begin
    declare v_temp  bit(16);
    set v_temp = 1;
    if new.node_id < 0 then 
        /*select '[trg_bef_node_inserted-error]:node_id cannot be negative!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_node_updated;;
create trigger trg_bef_node_updated after update on tb_node
for each row 
begin
    declare v_temp  bit(16);
    set v_temp = 1;
    if new.node_id < 0 then 
        /*select '[trg_bef_node_updated-error]:node_id cannot be negative!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_sensor_type_inserted;;
create trigger trg_bef_sensor_type_inserted before insert on tb_sensor_type 
for each row 
begin
    if fn_count_sensor(new.sensor_id) <> 1 then
        /*select '[trg_bef_sensor_type_inserted-error]:illegal sensor_id!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_sensor_type_updated;;
create trigger trg_bef_sensor_type_updated after update on tb_sensor_type 
for each row 
begin
    if fn_count_sensor(new.sensor_id) <> 1 then
        /*select '[trg_bef_sensor_type_updated-error]:illegal sensor_id!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_instance_inserted;;
create trigger trg_bef_instance_inserted before insert on tb_instance 
for each row 
begin

end;;

/*================================================*/
drop trigger if exists trg_record_inserted;;
create trigger trg_record_inserted before insert on tb_record 
for each row 
begin

end;;

delimiter ;

/**//*================================================*/
/*drop trigger if exists trg_student_myisam_base;;*/
/*create trigger trg_student_myisam_base after insert on student_myisam_base*/
/*for each row */
/*begin */
/*update student_innodb_base  */
/*set Sage = Sage + 1 */
/*where Sno = '001'; */
/*insert into tb_null values(1); */
/*end;;*/

/**//*================================================*/
/*drop trigger if exists trg_student_innodb_base;;*/
/*create trigger trg_student_innodb_base after insert on student_innodb_base*/
/*for each row */
/*begin */
/*update student_myisam_base  */
/*set Sage = Sage + 1 */
/*where Sno = '001'; */
/*insert into tb_null values(1); */
/*end;;*/

/**//*================================================*/
/*drop trigger if exists trg_student_innodb_subtb;;*/
/*create trigger trg_student_innodb_subtb after insert on student_innodb_subtb*/
/*for each row */
/*begin */
/*update student_innodb_base  */
/*set Sage = Sage + 1 */
/*where Sno = '001'; */
/*insert into tb_null values(1); */
/*end;;*/

/**//*================================================*/
/*drop trigger if exists trg_student_myisam_subtb;;*/
/*create trigger trg_student_myisam_subtb after insert on student_myisam_subtb*/
/*for each row */
/*begin */
/*update student_myisam_base  */
/*set Sage = Sage + 1 */
/*where Sno = '001'; */
/*insert into tb_null values(1); */
/*end;;*/
