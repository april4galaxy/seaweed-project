drop table if exists tb_record;
drop table if exists tb_instance;
drop table if exists tb_node;
drop table if exists tb_sensor_type;

create table if not exists tb_node (
    node_id         int             primary key,
    sensor_set      bit(16)	        not null,
    comments        varchar(100)    default null
    )ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;

create table if not exists tb_sensor_type(
    sensor_id       bit(16)        	primary key,
    sensor_name     char(30)        not null
    )ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;

create table if not exists tb_instance(
    instance_id     bigint          auto_increment primary key,  
    node_id         int             not null,
    sense_time      datetime        not null,
    /*sense_time      datetime        default current_timestamp on update         current_timestamp,*/
    /*primary key(instance_id, node_id, sense_time),*/
    foreign key(node_id) references tb_node(node_id) on update cascade 
    )ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;
    

create table if not exists tb_record(
    instance_id     bigint          not null,
    sensor_id       bit(16)         not null,
    data            double(10,2)    not null,
    primary key(instance_id,sensor_id),
    foreign key(instance_id) references tb_instance(instance_id) on update cascade,
    foreign key(sensor_id) references tb_sensor_type(sensor_id) on update cascade
    )ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;
