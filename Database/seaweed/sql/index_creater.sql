create index ix_instance            on tb_instance(node_id, sense_time);
create index ix_instance_sense_time on tb_instance(sense_time);
create index ix_record_sensor_id    on tb_record(instance_id, sensor_id, data);
create index ix_record_data         on tb_record(sensor_id,data);
create index ix_sensor_type_sensor  on tb_sensor_type(sensor_id,sensor_name);
create index ix_node_info           on tb_node(node_id, sensor_set);
