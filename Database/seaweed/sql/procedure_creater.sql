delimiter ;;
drop trigger if exists trg_bef_node_inserted;;
create trigger trg_bef_node_inserted before insert on tb_node
for each row 
begin
    declare v_temp  bit(16);
    set v_temp = 1;
    if new.node_id < 0 then 
        /*select '[trg_bef_node_inserted-error]:node_id cannot be negative!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_node_updated;;
create trigger trg_bef_node_updated after update on tb_node
for each row 
begin
    declare v_temp  bit(16);
    set v_temp = 1;
    if new.node_id < 0 then 
        /*select '[trg_bef_node_updated-error]:node_id cannot be negative!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_sensor_type_inserted;;
create trigger trg_bef_sensor_type_inserted before insert on tb_sensor_type 
for each row 
begin
    if fn_count_sensor(new.sensor_id) <> 1 then
        /*select '[trg_bef_sensor_type_inserted-error]:illegal sensor_id!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_sensor_type_updated;;
create trigger trg_bef_sensor_type_updated after update on tb_sensor_type 
for each row 
begin
    if fn_count_sensor(new.sensor_id) <> 1 then
        /*select '[trg_bef_sensor_type_updated-error]:illegal sensor_id!!!';*/
        insert into tb_null values(null);
    end if;
end;;

/*================================================*/
drop trigger if exists trg_bef_instance_inserted;;
create trigger trg_bef_instance_inserted before insert on tb_instance 
for each row 
begin

end;;

/*================================================*/
drop trigger if exists trg_record_inserted;;
create trigger trg_record_inserted before insert on tb_record 
for each row 
begin

end;;
/*================================================*/
drop procedure if exists sp_inser_node;;
create procedure sp_inser_node(in node_id int, in sensor_set bit(16), in comments varchar(100))
begin
    /*select 'inser_node';*/
    if comments is null then
        insert into tb_node(node_id, sensor_set) values (node_id, sensor_set);
    else
        insert into tb_node values (node_id, sensor_set, comments);
    end if;
end;;

    /**//*//////////////////////////////////////////////////*/
    /**//*// 无需标时，sensor_id自增*/
    /**//*//////////////////////////////////////////////////*/
    /*declare last_sensor_id  bit(16);*/
    /*set last_sensor_id      = 0;*/
    /*select max(sensor_id) from tb_sensor_type */
    /*into last_sensor_id;*/

    /*if last_sensor_id > 0 then*/
    /*insert into tb_sensor_type(sensor_id, sensor_name) values (last_sensor_id * 2,  sensor_name);*/
    /*select bin(sensor_id) from tb_sensor_type */
    /*where tb_sensor_type.sensor_name = sensor_name*/
    /*into v_sensor_id;*/
    /*else */
    /*insert into tb_sensor_type(sensor_id, sensor_name) values (1, sensor_name);*/
    /*set v_sensor_id = 1;*/
    /*end if;*/
    /**//*//////////////////////////////////////////////////*/
/*================================================*/
drop procedure if exists sp_inser_sensor_type;;
create procedure sp_inser_sensor_type(in sensor_id bit(16), in sensor_name char(30), out v_sensor_id bit(16))
lable_inser_sensor_type:begin
    declare amount int;
    declare temp_sensor_id int;
    set amount = 0;
    set temp_sensor_id = 0; 

    select count(*), sensor_id from tb_sensor_type
    where tb_sensor_type.sensor_id = sensor_id or tb_sensor_type.sensor_name = sensor_name into amount, temp_sensor_id;
    if amount = 0 then
        /*select 'inser_sensor_type', bin(sensor_id);*/
        insert into tb_sensor_type(sensor_id, sensor_name) values(sensor_id, sensor_name);
        set v_sensor_id = sensor_id;
    else 
        set v_sensor_id = sensor_id;
        leave lable_inser_sensor_type;
    end if;
end;;

/*================================================*/
drop procedure if exists sp_inser_instance;;
create procedure sp_inser_instance(in node_id int, in sense_time datetime, out v_instance_id bigint)
lable:begin
    set v_instance_id = -1;
    select instance_id from tb_instance
    where tb_instance.node_id = node_id and tb_instance.sense_time = sense_time
    into v_instance_id;
    if v_instance_id = -1 then
        /*select 'inser_instance';*/
        insert into tb_instance(node_id, sense_time) values (node_id, sense_time);
        select count(*) from tb_instance into v_instance_id;
        select instance_id from tb_instance 
        where tb_instance.node_id = node_id and tb_instance.sense_time = sense_time 
        into v_instance_id;
    end if;
end;;

/**//*================================================*/
drop procedure if exists sp_inser_record;;
create procedure sp_inser_record(in node_id int, in sensor_name char(30),in sense_time datetime, in data double(10,2))
lable:begin
    declare v_sensor_id         bit(16)     default 0;
    declare v_is_sensor_exist   smallint    default 0;
    declare v_is_node_exist     smallint    default 0;
    declare v_instance_id       bigint      default 1;
    declare v_sensor_set        bit(16)     default 0;
    /*Solving the warning : Warning: No data - zero rows fetched, selected, or processed*/
    /*DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;    */
    set v_instance_id       = 1;
    set v_is_node_exist     = 0;
    set v_is_sensor_exist   = 0;
    set v_sensor_id         = 0;
    set v_sensor_set        = 0;

    select count(*), sensor_id from tb_sensor_type
    where tb_sensor_type.sensor_name = sensor_name
    into v_is_sensor_exist, v_sensor_id;
    select count(*), sensor_set from tb_node
    where tb_node.node_id = node_id
    into v_is_node_exist, v_sensor_set;
    /*select node_id; */
    /*select v_is_node_exist, v_is_sensor_exist;*/
    if v_is_node_exist > 0 and v_is_sensor_exist > 0 then
        if v_sensor_set & v_sensor_id then 
            call sp_inser_instance(node_id, sense_time, v_instance_id);
            insert into tb_record(instance_id, sensor_id, data) values (v_instance_id, v_sensor_id, data); 
        else
            update tb_node
            set sensor_set = sensor_set | v_sensor_id
            where tb_node.node_id = node_id;
            call sp_inser_instance(node_id, sense_time, v_instance_id);
            insert into tb_record(instance_id, sensor_id, data) values (v_instance_id, v_sensor_id, data); 
        end if ; 
        leave lable;
    end if;
    if v_is_node_exist <= 0 and v_is_sensor_exist > 0 then 
        /*select 'here';*/
        call sp_inser_node(node_id, v_sensor_id, null);
        call sp_inser_instance(node_id, sense_time, v_instance_id);
        insert into tb_record(instance_id, sensor_id, data) values (v_instance_id, v_sensor_id, data); 
        leave lable;
    end if;
    if v_is_node_exist > 0 and v_is_sensor_exist <= 0 then
        select max(sensor_id) from tb_sensor_type into v_sensor_id;
        if v_sensor_id is not null then
            set v_sensor_id = v_sensor_id << 1;
            call sp_inser_sensor_type(v_sensor_id, sensor_name, v_sensor_id);
        else 
            call sp_inser_sensor_type(1, sensor_name, v_sensor_id);
        end if;

        update tb_node
        set sensor_set = sensor_set | v_sensor_id
        where tb_node.node_id = node_id;

        call sp_inser_instance(node_id, sense_time, v_instance_id);
        insert into tb_record(instance_id, sensor_id, data) values (v_instance_id, v_sensor_id, data);
        leave lable;
    end if;
    if v_is_node_exist <= 0 and v_is_sensor_exist <= 0 then
        select max(sensor_id) from tb_sensor_type into v_sensor_id;
        if v_sensor_id is not null then
            set v_sensor_id = v_sensor_id << 1;
            call sp_inser_sensor_type(v_sensor_id, sensor_name, v_sensor_id);
        else 
            call sp_inser_sensor_type(1, sensor_name, v_sensor_id);
        end if;
        call sp_inser_node(node_id, v_sensor_id, null);
        call sp_inser_instance(node_id, sense_time, v_instance_id);
        insert into tb_record(instance_id,sensor_id,data) values (v_instance_id, v_sensor_id, data);
        leave lable;
    end if;
end;;

/*================================================*/
/*return the number of sensor the sensor_set contains
/*================================================*/
drop function if exists fn_count_sensor;;
create function fn_count_sensor(sensor_set bit(16))
returns int no sql
begin
    declare count   int;
    set count       = 0;
    while sensor_set > 0 do
        if sensor_set & 1 then
            set count = count + 1;
        end if;
        set sensor_set = sensor_set >> 1;
    end while;
    return count;
end;;

/*================================================*/
drop procedure if exists sp_search_full_record;;
create procedure sp_search_full_record()
begin
    select * from (
        select distinct(tb_first.instance_id) as instance_id, tb_first.sense_time as sense_time, tb_first.node_id, bin(sensor_set) as sensor_set, bin(tb_first.first_sensor) as first_sensor, first_sensor_name, tb_first.first_data, 0 as second_sensor, null as second_sensor_name, -1000 as second_data from 
        (select tb_instance.instance_id as instance_id, node_id, sense_time, sensor_id as first_sensor, data as first_data from tb_instance left join tb_record using (instance_id)) as tb_first
        join tb_node on tb_first.node_id = tb_node.node_id
        join (select sensor_id, sensor_name as first_sensor_name from tb_sensor_type) as tb_sensor_type_first on tb_first.first_sensor = tb_sensor_type_first.sensor_id
        where fn_count_sensor(sensor_set) = 1 
        union
        select distinct(tb_first.instance_id) as instance_id, tb_first.sense_time as sense_time, tb_first.node_id, bin(sensor_set) as sensor_set, bin(tb_first.first_sensor) as first_sensor, first_sensor_name, tb_first.first_data, bin(tb_second.second_sensor) as second_sensor, second_sensor_name, tb_second.second_data from 
        (select tb_instance.instance_id as instance_id, node_id, sense_time, sensor_id as first_sensor, data as first_data from tb_instance left join tb_record using (instance_id)) as tb_first
        join 
        (select tb_instance.instance_id as instance_id, node_id, sense_time, sensor_id as second_sensor, data as second_data from tb_instance left join tb_record using (instance_id)) as tb_second 
        on tb_first.instance_id = tb_second.instance_id and tb_first.first_sensor < tb_second.second_sensor 
        join tb_node on tb_first.node_id = tb_node.node_id
        join (select sensor_id, sensor_name as first_sensor_name from tb_sensor_type) as tb_sensor_type_first on tb_first.first_sensor = tb_sensor_type_first.sensor_id
        join (select sensor_id, sensor_name as second_sensor_name from tb_sensor_type) as tb_sensor_type_second on tb_second.second_sensor = tb_sensor_type_second.sensor_id
    )as tb_end
    order by instance_id;

end;;

/*================================================*/
drop procedure if exists sp_search_certain_node_record;;
create procedure sp_search_certain_node_record(in node_id int, in start_time datetime, in end_time datetime, in sensor_name char(30))
lable:begin
    declare v_count_sensor  int;
    declare v_sensor_set    bit(16);
    set v_sensor_set    = 0;
    set v_count_sensor  = 0;

    set @node_id        = node_id;
    set @start_time     = start_time;
    set @end_time       = end_time;
    set @sensor_name    = sensor_name;

    prepare node_single_sensor_style from 
    'select tb_instance.instance_id, node_id, bin(sensor_set) as sensor_set, sense_time, sensor_name, sensor_id, data from
    tb_instance left join tb_record using(instance_id) left join tb_node using(node_id) left join tb_sensor_type using (sensor_id)
    where node_id = ? and sense_time >= ? and sense_time <= ?';

    prepare node_certain_sensor_style from 
    'select instance_id, node_id, bin(sensor_set) as sensor_set, sense_time, sensor_name, bin(tb_record.sensor_id) as sensor_id, data from 
    tb_instance left join tb_record using(instance_id) left join tb_node using(node_id) left join tb_sensor_type using (sensor_id) 
    where node_id = ? and sense_time >= ?  and sense_time <= ? and sensor_name = ?';

    prepare node_double_sensor_style from 
    'select tb_first.instance_id, node_id, bin(sensor_set) as sensor_set, sense_time, first_sensor_name, bin(first_sensor_id) as first_sensor_id, first_data, second_sensor_name, bin(second_sensor_id) as second_sensor_id, second_data from 
    (select tb_instance.instance_id as instance_id, tb_instance.node_id as node_id, sensor_set, sense_time, sensor_name as first_sensor_name, sensor_id as first_sensor_id, data as first_data from 
        tb_instance left join tb_record using(instance_id) left join tb_node using(node_id) left join tb_sensor_type using (sensor_id)
        where node_id = ? and sense_time >= ?  and sense_time <= ?) as tb_first 
    join 
    (select instance_id, sensor_name as second_sensor_name, sensor_id as second_sensor_id, data as second_data from 
        tb_instance left join tb_record using(instance_id) left join tb_sensor_type using(sensor_id)
        where node_id = ? and sense_time >= ? and sense_time <= ?) as tb_second
    on tb_first.instance_id = tb_second.instance_id and first_sensor_id < second_sensor_id';
    
    select sensor_set from tb_node 
    where tb_node.node_id = node_id into v_sensor_set;
    set v_count_sensor = fn_count_sensor(v_sensor_set);
    
    case  v_count_sensor
    when 2 then 
        if sensor_name is null then 
            execute node_double_sensor_style using @node_id, @start_time, @end_time, @node_id, @start_time, @end_time;
        else 
            execute node_certain_sensor_style using @node_id, @start_time, @end_time, @sensor_name;
        end if; 
    when 1 then 
        execute node_certain_sensor_style using @node_id ,@start_time, @end_time, @sensor_name;
    else 
        leave lable;
    end case;
end;;

/*================================================*/
drop procedure if exists sp_search_node_info;;
create procedure sp_search_node_info(in node_id int)
lable:begin
    declare v_sensor_set    bit(16);
    declare v_count_sensor  int;

    set @node_id        = node_id;
    set v_sensor_set    = 0;
    set v_count_sensor  = 0;

    prepare certain_node_single_sensor from 
    'select node_id, bin(sensor_id) as sensor_id, sensor_name from 
    tb_node join tb_sensor_type on tb_node.sensor_set = tb_sensor_type.sensor_id
    where node_id = ?';

    prepare certain_node_double_sensor from 
    ' select tb_first.node_id as node_id, sensor_set, first_sensor_name, first_sensor_id, second_sensor_name, second_sensor_id from 
    (select node_id, bin(sensor_set) as sensor_set, sensor_name as first_sensor_name, bin(sensor_id) as first_sensor_id from tb_node join tb_sensor_type on sensor_set & sensor_id = sensor_id where tb_node.node_id = ?) as tb_first 
    join 
    (select node_id, sensor_name as second_sensor_name, bin(sensor_id) as second_sensor_id from tb_node join tb_sensor_type on sensor_set & sensor_id = sensor_id
        where tb_node.node_id = ?) as tb_second
    on tb_first.node_id = tb_second.node_id and first_sensor_id < second_sensor_id';

    prepare uncertain_node_sensor from 
    ' select tb_first.node_id as node_id, sensor_set, first_sensor_name, first_sensor_id, null as second_sensor_name, -1000 as second_sensor_id from
    (select node_id, bin(sensor_set) as sensor_set, sensor_name as first_sensor_name, bin(sensor_id) as first_sensor_id from 
        tb_node join tb_sensor_type on sensor_set = sensor_id ) as tb_first 
    union
    select tb_first.node_id as node_id, sensor_set, first_sensor_name, first_sensor_id, second_sensor_name, second_sensor_id from 
    (select node_id, bin(sensor_set) as sensor_set, sensor_name as first_sensor_name, bin(sensor_id) as first_sensor_id from 
        tb_node join tb_sensor_type on sensor_set & sensor_id = sensor_id ) as tb_first 
    join 
    (select node_id, sensor_name as second_sensor_name, bin(sensor_id) as second_sensor_id from 
        tb_node join tb_sensor_type on sensor_set & sensor_id = sensor_id) as tb_second
    on tb_first.node_id = tb_second.node_id and first_sensor_id < second_sensor_id';

    if node_id is not null then
        select sensor_set from tb_node
        where tb_node.node_id = node_id into v_sensor_set;
        set v_count_sensor = fn_count_sensor(v_sensor_set);

        case v_count_sensor
        when 1 then
            execute certain_node_single_sensor using @node_id;
        when 2 then
            execute certain_node_double_sensor using @node_id, @node_id;
        else 
            leave lable;
        end case;
    else 
        execute uncertain_node_sensor ;
        leave lable;
    end if;
end;;

/*=================================================*/
drop procedure if exists sp_show_tb_node;
create procedure sp_show_tb_node()
begin 
    select node_id, bin(sensor_set) as sensor_set, comments from tb_node;
end;;

/*=================================================*/
drop procedure if exists sp_show_tb_sensor_type;
create procedure sp_show_tb_sensor_type()
begin
    select bin(sensor_id) as sensor_id, sensor_name from tb_sensor_type;
end;;

/*=================================================*/
drop procedure if exists sp_show_tb_instance;
create procedure sp_show_tb_instance()
begin
    select instance_id, sense_time, node_id from tb_instance;
end;;

/*=================================================*/
drop procedure if exists sp_show_tb_record;
create procedure sp_show_tb_record()
begin
    select instance_id, bin(sensor_id) as sensor_id, data from tb_record;
end;;

delimiter ;
