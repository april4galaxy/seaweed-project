#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext

from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response

from .forms import UserRegisterForm
# from .nori.forms import PondInfoForm, NodeInfoForm

def site_info(request):
    return {
        'title': '紫菜项目',
        'project_name': '紫菜项目',
        # 'contents': ["主页", "节点", "传感器", "关于", "联系"],
    }

def home(request):
    # return render_to_response('index.html',
    #                           context_instance=RequestContext(request, 
    #                                                           processors=[site_info]))
    user = request.user
    if not user.is_authenticated():
        html = '''
        <h1>welcome here. here is test!</h1>
        <a href="/accounts/login">login</a>
        <a href="/accounts/logout">logout</a>
        <a href="/accounts/register">register</a>
        '''
    else:
        html = '''
        <h1>welcome here. %s</h1>
        <p>You Join Date: %s</p> 
        <p>You Last Login: %s</p>
        <a href="/accounts/logout">logout</a>
        ''' % (user.username, user.date_joined, user.last_login)
    return HttpResponse(html)

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect('/accounts/profile/')
    else:
        form = UserRegisterForm()
    return render_to_response('registration/register.html', {
        'form': form,
    }, context_instance=RequestContext(request))


def user_profile_view(request):
    # if request.method == 'POST':
    #     form1 = PondInfoForm(request.POST)
    #     form2 = NodeInfoForm(request.POST)
    #     if form1.is_valid() and form2.is_valid():
    #         pond_name = form1.cleaned_data['pond_name']
    #         node_id = form2.cleaned_data['node_id']
    #         html = 'You submit: Pond Name is %s and Node ID is %s' % (pond_name, node_id)
    #         return HttpResponse(html)
    # else:
    #     form1 = PondInfoForm()
    #     form2 = NodeInfoForm()
    # return render_to_response('registration/profile.html',
    #                           {
    #                               'form1': form1,
    #                               'form2': form2,
    #                           },
    #                           context_instance = RequestContext(request))
    return HttpResponse("This is user profile view.")

# @login_required
# def change_password_view(request):
#     from django.contrib.auth.views import password_change
#     url = '/accounts/password/change/done/'
#     defaults = {
#         'post_change_redirect': url

#     }
#     defaults['template_name'] = 'password_change_form.html'
#     return password_change(request, **defaults)

# @login_required
# def change_password_done_view(request):
#     from django.contrib.auth.views import password_change_done


def create_playlist(request):
    from django.contrib import messages
    messages.add_message(request, messages.INFO, "Your playlist was added successfully!")
    return render_to_response('playlists/create.html', 
                              context_instance=RequestContext(request))

# from django.contrib.auth.models import User, Group, Permission
# from django.contrib.contenttypes.models import ContentType

# content_type = ContentType.objects.get(app_label='myapp', model='BlogPost')
# permission = Permission.objects.create(codename='can_publish',
#                                        name='Can Publish Posts',
#                                        content_type=content_type)
# user = User.objects.get(username='duke_nukem')
# group = Group.objects.get(name='wizard')
# group.permissions.add(permission)
# user.groups.add(group)
