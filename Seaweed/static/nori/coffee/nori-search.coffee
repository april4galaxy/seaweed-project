$ ->
  class Prompt
    constructor: (@id) ->
      
    show: (elt)->
      if elt
        $("##{elt}").removeClass('hide')
      else
        @$prompt.removeClass('hide')
      # console.log @top, @left, @height, @width, @position
      return
    hide: (elt)->
      if elt
        $("##{elt}").addClass('hide')
      else
        @$prompt.addClass('hide')
      return
    input: (e)->
      ###
      # 监听输入框的事件
      ###
      e.stopPropagation()
      return
    click: (e)->
      ###
      # 单击选项之后的处理
      # 1. 补全输入框中的内容
      # 2. prompt消失
      ###
      e.stopPropagation()
      self = @
      val = e.target.innerText
      @$id.val(val)
      self.hide()
      return
    hover: (e)->
      ###
      # 光标悬停在选项上<li></li>的处理事件
      # 高亮其背景
      ###
      e.stopPropagation()       #阻止事件的进一步传播
      # console.log(e)
      $target = $(e.target)
      $target.siblings().removeClass('hover') #先移出所有的hover
      $target.addClass('hover')
      return
    data: (data) ->
      html = ''
      for d in data
        html += "<li>#{d}</li>"
      $ul= @$prompt.find('ul')
      $ul.html(html)
      return
      
    ajax: (callback)->
      self = @
      name = @id.split('_')[1]
      url = "/nori/ajax/#{name}_ids/"
      get_ids = new Post url, (data) ->
        try
          data = JSON.parse(data) 
          # console.log data
        catch error
          # console.log error
          return
        self.data data
        return
        # console.log(data)
        # node_prompt.init()
        # node_prompt.data(data)
        # node_prompt.show()
      get_ids.send()
       
    create: ()->
      @$id = $(@id)
      @top = @$id.offset().top
      @left = @$id.offset().left
      @height = @$id.height()
      @width = @$id.width()
      # 提示框的位置
      @position =
        top: @top + @height + 4
        left: @left
      id = @id.split('_')[1]+'_prompt'
      html = "<div id=#{id} class='prompt hide'><ul></ul></div>"
      $(html).appendTo('body')
      @$prompt = $("##{id}")
      @$prompt.css
        position: "absolute"
        top: @position.top
        left: @position.left
        width: @width
        border: "1px solid #8e8e8e"
        background: "white"

      # 绑定一些事件
      self = @
      $id = @$id
      $id.blur ->
        # self.hide()
        return
      .focus ->
        self.ajax()
        self.show()
        
      ###
      # 元素prompt
      ###
      $ul = @$prompt.find('ul')
      $ul.click (e)->
        self.click(e)
      .mouseover (e) ->
        self.hover(e)
      return self

  node_prompt = new Prompt '#id_node_id'
  sensor_prompt = new Prompt '#id_sensor_id'
  node_prompt.create()
  sensor_prompt.create()

  # 日历
  $.fn.datepicker.defaults.language = "zh-CN"
  $.fn.datepicker.defaults.format = "yyyy-mm-dd"
  $('#id_start_time').datepicker()
  $('#id_end_time').datepicker()
  # new Dygraph $("#demodiv")[0], "/static/data/ny-vs-sf.txt",
  #   rollPeriod: 14  
  #   showRoller: true
  #   customBars: true
  #   title: 'NYC vs. SF'
  #   ylabel: 'Temperature (F)'
  #   legend: 'always'
  records_num = $('#search_table tbody').children().length
  if records_num > 0
    t_parse = new ParseTable 'search'
    t_parse.read('search_table')
    dy_draw = new DrawDygraphs t_parse.id
    dy_draw.load( t_parse.parse() )
    dy_draw.draw('search_chart')

  # g = new Dygraph $("#demodiv")[0],
  #   [
  #     [new Date("2013-10-27 14:00"),undefined,100]
  #     [new Date("2013-10-28 14:12"),20,80]
  #     [new Date("2013-10-29 12:12"),50,60]
  #     [new Date("2013-10-30 14:57"),70,80]
  #   ],
  #   {
  #     labels: ["x", "A", "B"]
  #   }
    
  $("#demodiv").mousedown (e)->
    switch(e.which)
      when 1
        console.log "left"
      when 2
        alert "middle"
      when 3
        alert "right"
      else
        alert "strange"
    
  $('#reset').click ->
    g.resetZoom()
  return
