$ ->
  
  draw_chart "realtime_table", "realtime_chart", 'Bar'
    
  t_parse = new ParseTable 'today'
  t_parse.read('history_table')
  dy_draw = new DrawDygraphs t_parse.id
  dy_draw.load( t_parse.parse() )
  dy_draw.draw('today_chart')


  # draw_chart "history_table", "today_chart", 'Line'
  $("#draw_history").click ->
    $("#history_chart").html("") #删除原绘图区域
    t_parse = new ParseTable 'history'
    t_parse.read('history_table')
    dy_draw = new DrawDygraphs t_parse.id
    dy_draw.load( t_parse.parse() )
    dy_draw.draw('history_chart')
    # draw_chart "history_table", "history_chart", 'Line'

  # 当页面加载完毕时运行
  # 滚条棍到底，自动加载10条数据
  # setTimeout ->
  #   isData = true                 #默认有数据 
  #   $(window).scroll ->
  #     if $(window).scrollTop() + $(window).height() is
  #           $(document).height()
  #       $load = $('#loading')
  #       $tbody = $('#history_table tbody')
  #       $load.html("正在加载数据，请稍等...")
  #       node_id = $('h1[node_id]').text()
  #       last_date = $('#history_table tbody tr:last-child td:last-child').html()
  #       end = parse_time(last_date)
  #       console.log end
  #       get_a_day = new Post "/nori/ajax/node/",
  #         (data) ->
  #           # console.log data
  #           if data isnt 'null'
  #             $tbody.append(data)
  #           else
  #             isData = false
  #             $load.html('已经没有数据了。')
  #             $(window).unbind('scroll')
  #       if isData
  #         get_a_day.send({node_id: node_id, end: end})
  #       return
  # , 0

  return
