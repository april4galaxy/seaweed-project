#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from .models import MyUser as User

class UserRegisterForm(UserCreationForm):
    phone = forms.RegexField(label=u"手机号码", max_length=11,
                                regex=r'^[\d]+$',
                                help_text=u"输入11位的手机号码",
                                error_messages={
                                    'invalid': u"只能输入11位的手机号码",})

    class Meta:
        model = User
        fileds = ("phone",)

    def clean_phone(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        phone = self.cleaned_data["phone"]
        try:
            User._default_manager.get(phone=phone)
        except User.DoesNotExist:
            return phone
        raise forms.ValidationError(self.error_messages[u'输入的手机号码无效'])

    def save(self, commit=True):
        user = super(UserRegisterForm, self).save(commit=False)
        user.set_phone(self.cleaned_data["phone"])
        if commit:
            user.save()
        return user
    pass
