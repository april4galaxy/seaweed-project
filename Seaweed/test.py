#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import sys
import random
import time
from datetime import datetime, timedelta

nodes = [1,4,2,3]
sensors = [101, 102, 104, 103]
global now 
a_day = timedelta(days = 1)
a_second = timedelta(seconds = 1)
a_minuts = timedelta(minutes = 1)

# from .settings.py import localdb as db
db = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'djangodb',
    'USER': 'djangouser',
    'PASSWORD': 'djangouser',

    # 'NAME': 'smartwin_cdma',
    # 'USER': 'smartwin',
    # 'PASSWORD': 'smartwin',
    'HOST': 'localhost',
    'PORT': '3306',
}

def create_history_data():
    now = datetime.now()               # 得到现在的时间
    # now = now - a_day
    try:
        print 'connect...'
        con = mdb.connect(db['HOST'], db['USER'], db['PASSWORD'], db['NAME'])
        cur = con.cursor()
        print cur.execute('select version()')
        counter = 0
        while True:
            counter = counter + 1
            if counter > 60:
                break;
            r = int(random.random() * 10 % 4)
            node_id = nodes[r]
            r = int(random.random() * 10 % 4)
            sensor_id = sensors[r]
            r = random.random() * 1000 + 12 / 3.0 - 1
            data = round(r * 100.0 / 20.0 + 10, 2)
            now = now + a_minuts
            insert_time = now.strftime('%Y-%m-%d %H:%M:%S')
            sql = 'insert into tb_history_data(node_id, sensor_id, data, insert_time) values(%d, %d, %f, "%s")' % (node_id, sensor_id, data, insert_time)
            try:
                cur.execute(sql)
                con.commit()
                time.sleep(1)
                # print cur.execute('select * from tb_history_data')
                print "Insert"
            except:
                print 'Except'
                sys.exit(1)
    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0], e.args[1])
        sys.exit(1)
    finally:
        if con:
            print "Close"
            con.close()
            

# def main():
#     try:
#         con = mdb.connect('forestgump.tk', 'smartwin', 'smartwin', 'smartwin_cdma')
#         # con = mdb.connect('127.0.0.1', 'root', 'njjizyj0826', 'django')
#         cur = con.cursor()
#         cur.execute('select version()')
#         ver = cur.fetchone()
#         print "Database version : %s " % ver
#     except mdb.Error, e:
#         print "Error %d: %s" % (e.args[0], e.args[1])
#         sys.exit(1)
#     finally:
#         if con:
#             con.close()
    
if __name__ == '__main__':
    # main()
    create_history_data()
