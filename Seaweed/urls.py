# -*- encoding: utf-8 -*-

from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.contrib.auth.views import login, logout, password_change, password_change_done

from django.views.generic import TemplateView, RedirectView
# from django.views.generic.simple import direct_to_template # deprecated

from Seaweed.books import views as books_views 
from Seaweed.contact import views as contact_views
from Seaweed.books.views import PublisherListView, AuthorListView, BookListView

from .views import register, user_profile_view, create_playlist

class TextPlainView(TemplateView):
    def render_to_response(self, context, **kwargs):
        return super(TextPlainView, self).render_to_response(
            context, content_type='text/plain', **kwargs)

urlpatterns = patterns('',
                       # (r'^searchform/$', views.search_form),
                       (r'^search/$', books_views.search),
                       (r'^contact/$', contact_views.contact),
                       (r'^contact/thanks', contact_views.thanks),
                       (r'^about/$', TemplateView.as_view(template_name='about.html')),

                       (r'^publishers/$', PublisherListView.as_view(template_name='publisher_list.html')),
                       (r'^authors/$', AuthorListView.as_view(template_name='author_list.html')),
                       (r'^books/$', BookListView.as_view(template_name='book_list.html')),
    # Examples:
                       url(r'^$', 'Seaweed.views.home', name='home'),
                       # (r'^$', TemplateView.as_view(template_name='index.html')),
    # url(r'^Seaweed/', include('Seaweed.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),

                       (r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico')),
                       url(r'^robots\.txt$', TextPlainView.as_view(template_name='robots.txt')),


                       # Account 
                       url(r'^accounts/login/$', login, {'template_name': "registration/login.html"}, name='my_login'),
                       url(r'^accounts/logout/$', logout, {'template_name': "registration/logout.html"}, name='my_logout'),
                       url(r'^accounts/profile/$', user_profile_view, name='my_profile'),
                       # url(r'^accounts/profile/$', TemplateView.as_view(template_name='registration/profile.html'), name='my_profile'),
                       url(r'^accounts/register/$', register, name='my_register'),
                       url(r'^accounts/password/change/$', password_change, name='my_password_change'),
                       url(r'^accounts/password/change/done/$', password_change_done, name='my_password_change_done'),
)

# 紫菜项目
urlpatterns += patterns('',
                        (r'^nori/', include('Seaweed.nori.urls')),
)
# test
urlpatterns += patterns('', 
                        (r'^playlist/create/$', create_playlist))
