#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.http import HttpResponse

import socket
import json

# HOST, PORT = "192.168.1.110", 
HOST, PORT = "127.0.0.1", 50007

def test_socket(request):
    message = {
        'node_id': '1',
        'action': 'on',
    }
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as msg:
        sock = None
        return HttpResponse("Error")
    try:
        sock.connect((HOST, PORT))
        sock.settimeout(2)
    except:                     # 连接发生异常
        return HttpResponse("Error")
        # pass
    else:                       # 连接正常
        r = json.dumps(message)
        sock.sendall(r)
        # time.sleep(2)
        rec = sock.recv(100)
        print rec
        return HttpResponse(rec)
    finally:
        sock.close()

if __name__ == '__main__':
    while True:
        test_socket('1123')

