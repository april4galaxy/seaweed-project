# -*- encoding: utf-8 -*-
from django import template
from django.http import Http404
from django.template.base import Variable, Library, VariableDoesNotExist

register = Library()

d_sensor_name = {
    "101": "LIGHT",
    "102": "TEMPERATURE",
    "103": "FLOWSPEED",
    "104": "HUMIDITY",
}

d_sensor_cn_name = {
    "101": "光照传感器",
    "102": "温度传感器",
    "103": "流速传感器",
    "104": "浊度传感器",
}

@register.filter(name='id_to_cn_name')
def id_to_cn_name(value):
    return d_sensor_cn_name[str(value)]
    # try:
    #     return d_sensor_cn_name[str(value)]
    # except:
    #     raise Http404()
