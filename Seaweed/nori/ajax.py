#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template

import json
from django.core import serializers

# from .models import TbHistoryData, TbRealtimeData
# 导入所有以节点为单位的处理业务函数
from .node_handle import *

def ajaxNode(request):
    end = request.POST['end']
    node_id = request.POST['node_id']
    end = end.split(" ")[0] # 将日期拿出来
    data = get_data_bf_a_day(node_id, end)
    if len(data) > 0:
        t = get_template('tr.html')
        html = t.render(Context({'nodes': data}))
    else:
        html = 'null'
    # data = serializers.serialize('json', data)
    # print "*" * 20
    # print data
    return HttpResponse(html)
    # get_records

def ajaxNodeIds(request):
    data = get_all_hd_node_id()
    # data is <class 'django.db.models.query.ValuesListQuerySet'>
    # so has to convert it to list object
    data_list = list(data)
    try:
        msg = json.dumps(data_list)
    except:
        return HttpResponse("发生了一些意想不到的错误")
    return HttpResponse(msg)

def ajaxSensorIds(request):
    data = get_all_hd_sensor_id()
    data_list = list(data)
    print type(data_list)
    try:
        msg = json.dumps(data_list)
    except:
        print u"发生了一些意想不到的错误"
        return HttpResponse("发生了一些意想不到的错误")
    print msg
    return HttpResponse(msg)
