# -*- coding: utf-8 -*-

# Create your views here.
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
# 导入所有以节点为单位的处理业务函数
from .node_handle import *
from .forms import SearchForm

def site_info(request):
    return {
        'title': '紫菜项目',
        'project_name': '紫菜项目',
        'contents': (
            ("home", "主页"), 
            ("node", "节点"), 
            ("search", "查询"), 
            # ("sensor", "传感器"), 
            # ("control", "控制"),
            ("about", "关于"), 
            ("contact", "联系"),
        ),
    }

def home(request):
    return render_to_response('nori-index.html',
        context_instance=RequestContext(request, processors=[site_info])
    )

def about_view(request):
    return render_to_response('nori-about.html',
        context_instance=RequestContext(request, processors=[site_info])
    )

@login_required
def search_view(request):
    records = []
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            a = {}
            a['node_id'] = form.cleaned_data['node_id']
            a['sensor_id'] = form.cleaned_data['sensor_id']
            a['start_time'] = form.cleaned_data['start_time']
            a['end_time'] = form.cleaned_data['end_time']
            try:
                records = search_history(a)
            except:
                HttpResponse('SQL query errors')
    else:
        form = SearchForm()
    return render_to_response("nori-search.html",
                              {"form": form,
                               "records": records},
        context_instance=RequestContext(request, processors=[site_info]))

# TODO: resolved!
# !Deprecated
def dispatch(request, dist=None, node_id=None):
    print dist
    handler = {
        "node": nodeView,
        "sensor": sensorView,
        "control": controlView,
    }
    try:
        return handler[dist](request, node_id)
    except:
        raise Http404()

# node视图的处理
@login_required
def node_view(request, node_id=None):
    try:
        node_id = int(node_id)
    except:
        # 说明是node主页或者任何其他错误
        try: 
            nodes = get_all_realtime_data()
        except:
            nodes = []
        return render_to_response(
            'nori-nodes.html', 
            {
                "nodes": nodes,
            },
            context_instance=RequestContext(request, processors=[site_info])
        )
    else:
        # nodes = get_history_data(node_id)
        nodes = get_today_data_by_node_id(node_id)
        realtime_data = get_realtime_data_by_node_id(node_id)
        if nodes and realtime_data:               # 说明所查的节点存在
            return render_to_response(
                'nori-nodes-item.html', 
                {
                    "node_id": node_id,
                    "realtime_data": realtime_data,
                    "nodes": nodes
                },
                context_instance=RequestContext(request, processors=[site_info])
            )
        else:
            raise Http404()

def sensor_view(request, sensor_id=0):
    return HttpResponse("Here is sensor info")

def control_view(request, node_id=0):
    # from .socket_handle import *
    # info = test_socket(request)
    # info = "Nothing"
    info = '''
    <a href="test/socket/">Test Socket</a>
    '''
    return HttpResponse("Here is control info<br />" + info)
# gumpstudio#110

def getTodaydata(request):
    data = get_today_data_by_sensor_id(101)
    # data = get_today_all_data()
    return HttpResponse(data)

from django.core import serializers
def test(request):
    nodes = serializers.serialize('json', get_all_realtime_data2())
    return HttpResponse(nodes)
