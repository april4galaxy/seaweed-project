from django.contrib import admin
from .models import TbHistoryData, TbRealtimeData

admin.site.register(TbHistoryData)
admin.site.register(TbRealtimeData)
