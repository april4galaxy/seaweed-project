# -*- encoding: utf-8 -*-
from django import template
from django.template.base import Variable, Library, VariableDoesNotExist

from Seaweed.nori.head.py import d_sensor_name

register = Library()

@register.filter(name='id_to_name')
def id_to_name(value, arg):
    return d_sensor_name[value]
