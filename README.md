# This is my README

* Deploy Development Environment
** Installation
1. SQL
   + MySQL

2. Service
   + python
   + django

3. Web
   + nodejs(npm)
   + grunt
   + bower

** Deploy
1. SQL
   #+begin_src sh
     # mysqld
   #+end_src
2. Service
   #+BEGIN_SRC sh
     # python managy.py sync
     # python managy.py runserver
   #+END_SRC
3. Web
   #+begin_src sh
     # bower install
     # npm install
     # grunt
     # coffee -o lib/* -cwb coffee/*
   #+end_src
